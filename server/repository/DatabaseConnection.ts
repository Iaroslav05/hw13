import { DataSource } from "typeorm";
import { config } from "dotenv";

config();

class DatabaseConnection {
  private static instance: DatabaseConnection;
  private db: DataSource;
  private constructor() {
    this.db = new DataSource({
      type: "postgres",
      host: process.env.DB_HOST || "localhost",
      port: Number(process.env.DB_PORT) || 5432,
      username: process.env.DB_USER || "postgres",
      password: process.env.DB_PASS || "rootroot",
      database: process.env.DB_NAME || "typeorm",
      synchronize: true,
      logging: false,
      entities: [`${__dirname}/../entity/*.ts`],
      migrations: [],
      subscribers: [],
    });
	 this.db.initialize()
  }
  public static getInstance(): DatabaseConnection {
    if (!DatabaseConnection.instance) {
      DatabaseConnection.instance = new DatabaseConnection();
    }
    return DatabaseConnection.instance;
  }

  public async getDB() {
	
    try {
     if (this.db.isInitialized) {
       return this.db;
     }
	  console.log("Data Source has been initialized!");
    } catch (err) {
      console.error("Error during Data Source initialization", err);
    }
    return this.db;
  }
}

export default DatabaseConnection.getInstance();
